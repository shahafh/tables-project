import React from 'react';
import PropTypes from 'prop-types';
import { IoIosAlert } from 'react-icons/io';
import styles from './Comment.module.scss';
import Modal from './../components/Modal';
import useToggle from './useToggle';

const Comment = ({ children }) => {
  const { isOn, toggleOff, toggleOn } = useToggle();
  return (
    <div className={styles.container}>
      {isOn && (
        <Modal hideModal={toggleOff}>
          <span>pie</span>
          <button onClick={toggleOff} type="button">
            close
          </button>
        </Modal>
      )}
      <p className={styles.content}>{children}</p>
      <div>
        <IoIosAlert onClick={toggleOn} className={styles.icon} />
      </div>
    </div>
  );
};

Comment.propTypes = {
  children: PropTypes.string.isRequired,
};

export default Comment;
