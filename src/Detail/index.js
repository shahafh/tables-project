import React from 'react';
import PropTypes from 'prop-types';
import Card from './../components/Card';
import styles from './styles.module.scss';
import Comment from './Comment';
import Table from './../components/Table';
import Progress from '../components/Progress';

const Detail = ({ data, tableId }) => {
  const table = data[tableId];
  return (
    <Card className={styles.detailContainer} title={table.name}>
      <Progress
        className={styles.stagesContent}
        stages={Object.values(table.stages)}
      />
      <div className={styles.tablesContent}>
        <Comment type={table.comment.type}>{table.comment.content}</Comment>
        <div className={styles.tables}>
          <Table
            name="Total Relations"
            dataRows={table.relationsTable}
            title={`Total Relations: ${table.totalRelations}`}
          />
          <Table
            name="Total Entities"
            dataRows={table.entitiesTable}
            title={`Total Entities: ${table.totalEntities}`}
          />
        </div>
      </div>
    </Card>
  );
};

Detail.propTypes = {
  tableId: PropTypes.number.isRequired,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      stages: PropTypes.objectOf(
        PropTypes.shape({
          name: PropTypes.string.isRequired,
          status: PropTypes.oneOf(['DONE', 'FAIL', 'CURRENT', 'WAITING']),
          startTime: PropTypes.number.isRequired,
          endTime: PropTypes.number.isRequired,
        })
      ).isRequired,
      comment: PropTypes.shape({
        type: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired,
      }).isRequired,
      relationsTable: PropTypes.arrayOf(
        PropTypes.shape({
          type: PropTypes.string.isRequired,
          expected: PropTypes.number.isRequired,
          processed: PropTypes.number.isRequired,
        })
      ).isRequired,
      totalRelations: PropTypes.number.isRequired,
      totalEntities: PropTypes.number.isRequired,
      entitiesTable: PropTypes.arrayOf(
        PropTypes.shape({
          type: PropTypes.string.isRequired,
          expected: PropTypes.number.isRequired,
          processed: PropTypes.number.isRequired,
        })
      ).isRequired,
    })
  ).isRequired,
};

export default Detail;
