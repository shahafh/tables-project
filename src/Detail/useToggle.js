import { useState, useCallback } from 'react';

export default (initialState = false) => {
  const [isOn, setOn] = useState(initialState);
  const toggleOn = useCallback(() => setOn(true), [setOn]);
  const toggleOff = useCallback(() => setOn(false), [setOn]);
  const toggle = useCallback(() => setOn((state) => !state), [setOn]);
  return {
    isOn,
    toggleOn,
    toggleOff,
    toggle,
  };
};
