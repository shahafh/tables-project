import React from 'react';
import styles from './Header.module.scss';
import Box from './components/Card/Box';

const Header = () => (
  <Box className={styles.container}>
    <h1>App Logo</h1>
  </Box>
);

export default Header;
