import React from 'react';
import PropTypes from 'prop-types';

const Row = ({ tableName, content, rowIdx }) => (
  <tr>
    {Object.entries(content).map(([column, value]) => (
      <td key={`${tableName}-${rowIdx}-${column}`}>{value}</td>
    ))}
  </tr>
);

Row.propTypes = {
  content: PropTypes.object.isRequired,
  rowIdx: PropTypes.number.isRequired,
  tableName: PropTypes.string.isRequired,
};

export default Row;
