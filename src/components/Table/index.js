import React from 'react';
import PropTypes from 'prop-types';
import Row from './Row';

const Table = ({ name, dataRows, title }) => {
  const headers = Object.keys(dataRows[0]);
  return (
    <table>
      <caption>{title}</caption>
      <thead>
        <tr>
          {headers.map((header) => (
            <th key={`table-${name}-header-${header}`}>{header}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {dataRows.map((row, rowIdx) => (
          <Row
            key={`${name}-${row.type}-${row.expected}-${row.processed}`}
            tableName={name}
            rowIdx={rowIdx}
            content={row}
          />
        ))}
      </tbody>
    </table>
  );
};

Table.propTypes = {
  name: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  dataRows: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    )
  ).isRequired,
};

export default Table;
