import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';
import Box from './Box';

const Card = ({ title, children, transparent, className, status, noWrap }) => (
  <div className={styles.container}>
    <h2 className={styles.title}>{title}</h2>
    {noWrap ? (
      children
    ) : (
      <Box className={className} status={status} transparent={transparent}>
        {children}
      </Box>
    )}
  </div>
);

Card.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
  transparent: PropTypes.bool,
  className: PropTypes.string,
  status: PropTypes.oneOf(['SUCCESS', 'FAIL']),
  noWrap: PropTypes.bool,
};

Card.defaultProps = {
  transparent: false,
  className: '',
  status: null,
  noWrap: false,
};

export default Card;
