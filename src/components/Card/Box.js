import React from 'react';
import PropTypes from 'prop-types';
import cs from 'classnames';
import styles from './styles.module.scss';

const Box = ({ children, className, transparent, status, onClick }) => (
  // eslint-disable-next-line
  <div
    className={cs(styles.card, {
      [styles.transparent]: transparent,
      [styles.success]: status === 'SUCCESS',
      [styles.fail]: status === 'FAIL',
      [className]: className,
    })}
    onClick={onClick}>
    {children}
  </div>
);
Box.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
  transparent: PropTypes.bool,
  className: PropTypes.string,
  status: PropTypes.oneOf(['SUCCESS', 'FAIL']),
  onClick: PropTypes.func,
};

Box.defaultProps = {
  transparent: false,
  className: '',
  status: null,
  onClick: undefined,
};

export default Box;
