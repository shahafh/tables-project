import React, { useEffect, useCallback } from 'react';
import ReactDom from 'react-dom';
import PropTypes from 'prop-types';
import Box from './../Card/Box';
import styles from './styles.module.scss';

const body = document.getElementsByTagName('body')[0];
const el = document.createElement('div');
el.id = 'modal-root';

const Modal = ({ children, hideModal, onClick }) => {
  useEffect(() => {
    body.appendChild(el);
    return () => body.removeChild(el);
  });

  const handleClick = useCallback(
    (e) => {
      e.stopPropagation();
      if (onClick) onClick();
    },
    [onClick]
  );

  return ReactDom.createPortal(
    // eslint-disable-next-line
    <div className={styles.modalContainer} onClick={hideModal}>
      <Box className={styles.modalCard} onClick={handleClick}>
        {children}
      </Box>
    </div>,
    el
  );
};

Modal.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
  onClick: PropTypes.func,
};

Modal.defaultProps = {
  onClick: undefined,
};

export default Modal;
