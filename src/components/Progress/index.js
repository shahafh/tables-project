import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import cs from 'classnames';
import styles from './styles.module.scss';

const Progress = ({ stages, className }) => (
  <ul className={cs(styles.progress, { [className]: className !== '' })}>
    {stages.map((stage, idx) => (
      <li
        key={stage.name}
        className={styles.stage}
        data-bar-color={stage.status}
        data-with-bar={idx !== 0}>
        <p className={styles.content}>
          <span className={styles.contentRow}>
            {moment.unix(stage.startTime).format('lll')}
          </span>
          {/* <span className={styles.contentRow}>
            {moment.unix(stage.startTime).format('lll')}
          </span>
          <span className={styles.contentRow}>
            {moment.unix(stage.startTime).format('lll')}
          </span> */}
        </p>
      </li>
    ))}
  </ul>
);

Progress.propTypes = {
  stages: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      status: PropTypes.oneOf(['DONE', 'FAIL', 'CURRENT', 'WAITING'])
        .isRequired,
      startTime: PropTypes.number.isRequired,
      endTime: PropTypes.number.isRequired,
    })
  ).isRequired,
  className: PropTypes.string,
};

Progress.defaultProps = {
  className: '',
};

export default Progress;
