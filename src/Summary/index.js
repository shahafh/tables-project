import React from 'react';
import PropTypes from 'prop-types';
import Card from './../components/Card';
import styles from './styles.module.scss';
import Box from '../components/Card/Box';

const Summary = ({ data }) => (
  <Card
    className={styles.container}
    title={`ingestion ${data.ingestionName}`}
    transparent>
    <Box status={data.summaryStatus} className={styles.statusBox}>
      <h2 className={styles.internalText}>{`Status: ${data.summaryStatus}`}</h2>
      <p className={(styles.description, styles.internalText)}>
        {`Started At: ${data.startedAt},Finished At: ${data.finishedAt}`}
      </p>
      <p className={styles.internalText}>
        {`Total Duration: ${data.totalDuration}`}
      </p>
    </Box>

    <aside className={styles.errorDescription}>
      <p>{data.errorDescription}</p>
    </aside>
  </Card>
);

Summary.propTypes = {
  data: PropTypes.shape({
    ingestionName: PropTypes.string.isRequired,
    summaryStatus: PropTypes.oneOf(['SUCCESS', 'FAIL']).isRequired,
    startedAt: PropTypes.string.isRequired,
    finishedAt: PropTypes.string.isRequired,
    totalDuration: PropTypes.number.isRequired,
    errorDescription: PropTypes.string,
  }),
};

Summary.defaultProps = {
  data: PropTypes.shape({
    errorDescription: '',
  }),
};

export default Summary;
