import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';

const convertTimestamp = (timestamp) => {
  const date = new Date(Number(timestamp));
  const dateStr = date.toLocaleString();
  return dateStr.substr(0, dateStr.length - 3).replace(',', '');
};

const dataSourceProps = {
  name: PropTypes.string.isRequired,
  startTime: PropTypes.number.isRequired,
  endTime: PropTypes.number.isRequired,
  totalRelations: PropTypes.number.isRequired,
  totalEntities: PropTypes.number.isRequired,
  currentStage: PropTypes.string.isRequired,
};

const DataSourceItem = ({
  name,
  startTime,
  endTime,
  totalRelations,
  totalEntities,
  currentStage,
}) => (
  <li className={styles.item} table-stage={currentStage}>
    <h2 className={styles.header}>{name}</h2>
    <p className={styles.content}>
      {`start: ${convertTimestamp(startTime)}, 
     end: ${convertTimestamp(endTime)}`}
    </p>
    <p className={styles.content}>
      {`${totalRelations} relations, ${totalEntities} entities`}
    </p>
  </li>
);

DataSourceItem.propTypes = dataSourceProps;

export default DataSourceItem;
export { dataSourceProps };
