import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';
import DataSourceItem, { dataSourceProps } from './DataSourceItem';
import Card from './../components/Card';

const DataSources = ({ tables }) => (
  <Card className={styles.dataSourcesCard} title="Data Sources" noWrap>
    <div className={styles.dataSourcesContainer}>
      <ul className={styles.dataSources}>
        {tables.map((item) => (
          <DataSourceItem
            key={item.name}
            name={item.name}
            startTime={item.startTime}
            endTime={item.endTime}
            totalRelations={item.totalRelations}
            totalEntities={item.totalEntities}
            currentStage={item.currentStage}
          />
        ))}
      </ul>
    </div>
  </Card>
);

DataSources.propTypes = {
  tables: PropTypes.arrayOf(PropTypes.shape(dataSourceProps)).isRequired,
};

export default DataSources;
