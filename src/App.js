import React from 'react';
import PropTypes from 'prop-types';
import styles from './App.module.scss';
import Header from './Header';
import Summary from './Summary';
import DataSources from './DataSources';
import Detail from './Detail';

const App = ({ data }) => (
  <>
    <Header />
    <Summary data={data.summaryData} />
    <div className={styles.tablesContainer}>
      <DataSources tables={data.tablesData} />
      <Detail data={data.tablesData} tableId={1} />
    </div>
  </>
);

App.propTypes = {
  data: PropTypes.shape({
    tablesData: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
      })
    ).isRequired,
    summaryData: PropTypes.shape({
      ingestionName: PropTypes.string.isRequired,
      summaryStatus: PropTypes.string.isRequired,
      startedAt: PropTypes.string.isRequired,
      finishedAt: PropTypes.string.isRequired,
      totalDuration: PropTypes.number.isRequired,
      errorDescription: PropTypes.string,
    }).isRequired,
  }).isRequired,
};

Summary.defaultProps = {
  data: PropTypes.shape({
    summaryData: PropTypes.shape({
      errorDescription: '',
    }),
  }),
};

export default App;
