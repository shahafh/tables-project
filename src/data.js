const STATUS_VALUES = ['DONE', 'FAIL', 'CURRENT', 'WAITING'];
const createTable = (name, currentStage) => ({
  name,
  startTime: 1591264462,
  endTime: 1591264482,
  totalRelations: 2000,
  totalEntities: 10000,
  currentStage,
  stages: ['stage_a', 'stage_b', 'stage_c', 'stage_d', 'stage_e']
    .map((item) => ({
      name: item,
      status: STATUS_VALUES[Math.floor(Math.random() * STATUS_VALUES.length)],
      startTime: 1591264462,
      endTime: 1591264482,
    }))
    .reduce((acc, item) => ({ ...acc, [item.name]: item }), {}),
  relationsTable: [
    {
      type: 'Person',
      expected: 5000,
      processed: 4500,
    },
    {
      type: 'Device',
      expected: 10000,
      processed: 10000,
    },

    {
      type: 'Whatever',
      expected: 7500,
      processed: 7500,
    },
  ],

  entitiesTable: [
    {
      type: 'Person',
      expected: 5000,
      processed: 4500,
    },

    {
      type: 'Device',
      expected: 10000,
      processed: 10000,
    },

    {
      type: 'Whatever',
      expected: 7500,
      processed: 7500,
    },
  ],

  comment: {
    type: '',
    content:
      'comments comments comments comments comments comments comments comments comments comments',
  },
});

const data = {
  summaryData: {
    ingestionName: '<Name>',
    summaryStatus: 'FAIL', // SUCCESS / FAIL
    startedAt: '14/05/2020 14:00', // timestamp to convert later
    finishedAt: '14/05/2020 16:00',
    totalDuration: 2, // calculated field later
    errorDescription:
      'some stack trace ERROR some stack trace ERROR some stack trace ERROR some stack trace ERROR some stack trace ERROR some stack trace ERROR some stack trace ERROR',
  },

  tablesData: [
    createTable('Table 1', 'stage_a'),
    createTable('Table 2', 'stage_b'),
    createTable('Table 3', 'stage_c'),
    createTable('Table 4', 'stage_d'),
    createTable('Table 5', 'stage_e'),
    createTable('Table 6', 'stage_e'),
    createTable('Table 7', 'stage_e'),
  ],
};

export default data;
